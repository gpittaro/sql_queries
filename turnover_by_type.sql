SELECT 
  date,
  client_id,
  SUM(IF(product_type = 'MEUBLE', prod_price*prod_qty, 0)) vente_meuble, 
  SUM(IF(product_type = 'DECO', prod_price*prod_qty, 0)) vente_deco
FROM TRANSACTIONS as t
  INNER JOIN PRODUCT_NOMENCLATURE as pn 
  ON t.prod_id = pn.product_id
WHERE date BETWEEN DATE('2019-01-01') AND DATE('2019-12-31')
GROUP BY date, client_id
ORDER BY date
