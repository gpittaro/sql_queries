# SQL Queries

## Table description

We want to execute 2 queries on a set of 2 tables to get informations
about the company's turnover and which products sell best.

### Transactions

This table has data about the sales. It contains the following fields :
- date : when the sale has been made
- order_id : unique identifier for the order
- client_id : unique user identifier
- prod_id : unique product identified
- prod_price : unit price of the product
- prod_qty : quantity bought

Extract from the table `TRANSACTIONS` :

date|order_id|client_id|prod_id|prod_price|prod_qty
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
01/01/20|1234|999|490756|50|1
01/01/20|1234|999|389728|3|56
01/01/20|3456|845|490756|50|2
01/01/20|3456|845|549380|300|1
01/01/20|3456|845|293718|10|6


### Product nomenclature

This table has data about the products. It contains the following fields :
- product_id : unique product identifier
- product_type : product type (DECO or MEUBLE) 
- product_name : the product's name


Extract from the table `TRANSACTIONS` :

product_id|product_type|product_name
:-----:|:-----:|:-----:
490756|MEUBLE|Chaise
389728|DECO|Boule de Noël
549380|MEUBLE|Canapé
293718|DECO|Mug 

## Queries

### Turnover by day

File:

    turnover_by_day.sql

This query is quite straightforward. I selected the date for the first column. 

For the second I took the product of the product price and the quantity bought for each row and then summed them. 

I grouped by date and ordered by date as well. 

The query is limited to the year 2019 and gives us a history of sales by day for this period.

### Turnover by type

File:

    turnover_by_type.sql

This query was a bit more complex. 

It has a lot of similarities with the first, however we want to get sales for furnitures and decoration items on the same rows. 

This requires to pivot the table. I did that using a conditional sum.

The query checks the type of product and adds the total price (quantity\*price) for that row to the corresponding column, or 0.

This gives us a table with the turnover per client, per type of item, per day. I decided to sort it by day.


### Note

The queries have been tested on BigQuery.